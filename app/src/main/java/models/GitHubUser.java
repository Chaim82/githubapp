package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chaim on 22/05/2018.
 */

public class GitHubUser {

    @SerializedName("login")
    private String mLogin;
    @SerializedName("id")
    private int mId;
    @SerializedName("avatar_url")
    private String mUrl;
    @SerializedName("html_url")
    private String mHtml;

    public GitHubUser(String login, int id, String url, String html) {
        this.mLogin = login;
        this.mId = id;
        this.mUrl = url;
        this.mHtml = html;
    }

    public String getLogin() {
        return mLogin;
    }

    public int getId() {
        return mId;
    }

    public String getAvatarUrl() {
        return mUrl;
    }

    public String getHtml() {
        return mHtml;
    }
}
