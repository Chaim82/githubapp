package services;

/**
 * Created by Chaim on 22/05/2018.
 */

public interface IServiceResponseListener <T>{
    void onComplete(T result);
    void onError();
}
