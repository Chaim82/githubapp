package services;

import java.util.List;

import models.GitHubUser;

/**
 * Created by Chaim on 22/05/2018.
 */

public interface IGitHubServices {
    void getGitHubUsers(IServiceResponseListener<List<GitHubUser>> listener, int since);
}
