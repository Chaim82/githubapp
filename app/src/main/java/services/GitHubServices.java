package services;

import java.util.List;

import models.GitHubUser;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Chaim on 22/05/2018.
 */

public class GitHubServices implements IGitHubServices {

    private IGitHubApi mGithubApi;

    public GitHubServices() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

         mGithubApi = retrofit.create(IGitHubApi.class);
    }

    @Override
    public void getGitHubUsers(final IServiceResponseListener<List<GitHubUser>> listener, int since) {
       Call<List<GitHubUser>> usersCall = mGithubApi.loadUsers(since);
        usersCall.enqueue(new Callback<List<GitHubUser>>() {
            @Override
            public void onResponse(Call<List<GitHubUser>> call, Response<List<GitHubUser>> response) {
                if(!response.isSuccessful())
                    listener.onError();
                else {
                    listener.onComplete(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<GitHubUser>> call, Throwable t) {
                listener.onError();
            }
        });
    }
}
