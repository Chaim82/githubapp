package services;

import java.util.List;

import models.GitHubUser;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Chaim on 22/05/2018.
 */

public interface IGitHubApi {
    @GET("users")
    Call<List<GitHubUser>> loadUsers(@Query("since") int since);
}
