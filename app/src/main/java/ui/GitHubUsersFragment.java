package ui;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.test.githubapp.R;

public class GitHubUsersFragment extends Fragment implements Observer<GitHubUsersViewModel.UsersLoadingState>{

    private GitHubUsersViewModel mGitHubUsersViewModel;
    private RecyclerView mUserList;
    private ProgressBar mProgressBar;
    private GithubUsersAdapter mAdapter;
    private Context mContext;
    private int mFirstVisibleUserIndex = 0;

    private final String FirstVisibleUserIndexBundleKey = "FirstVisibleUserIndexBundleKey";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.github_users_fragment_layout, null);
        mProgressBar = view.findViewById(R.id.progressBar);
        initList(view);
        return view;
    }

    private void initList(View view) {
        mUserList = view.findViewById(R.id.users_list);
        LinearLayoutManager layoutManager = new  LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mAdapter = new GithubUsersAdapter(mContext);
        mUserList.setAdapter(mAdapter);
        mUserList.setLayoutManager(layoutManager);
        mUserList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastCompletelyVisibleItemPosition = 0;

                lastCompletelyVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();

                if(lastCompletelyVisibleItemPosition == mGitHubUsersViewModel.getUserLoadingState().getValue().mUsers.size()-1){
                    mGitHubUsersViewModel.loadNext();
                }

                mFirstVisibleUserIndex = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                if(mFirstVisibleUserIndex == mGitHubUsersViewModel.getFirstUserIndex()){
                    mGitHubUsersViewModel.loadPrev();
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGitHubUsersViewModel = ((GitHubUsersViewModelProvider)mContext).getGitHubUsersViewModel();
        mFirstVisibleUserIndex = savedInstanceState == null ? 0 : savedInstanceState.getInt(FirstVisibleUserIndexBundleKey,0);
        mGitHubUsersViewModel.getUserLoadingState().observe(this, this);
        mGitHubUsersViewModel.start(mFirstVisibleUserIndex);
    }

    @Override
    public void onChanged(@Nullable GitHubUsersViewModel.UsersLoadingState usersLoadingState) {
        switch (usersLoadingState.mState){
            case GitHubUsersViewModel.UsersLoadingState.IS_LOADING:
                mProgressBar.setVisibility(View.VISIBLE);
                mUserList.setVisibility(View.GONE);
                break;
            case GitHubUsersViewModel.UsersLoadingState.IS_READY:
                mProgressBar.setVisibility(View.GONE);
                mUserList.setVisibility(View.VISIBLE);
                mAdapter.addUsers(mGitHubUsersViewModel.getFirstUserIndex(), usersLoadingState.mUsers, usersLoadingState.mUsersFromLastUpdate);
                break;
            case GitHubUsersViewModel.UsersLoadingState.IS_LOADING_NEXT:
                mProgressBar.setVisibility(View.GONE);
                mUserList.setVisibility(View.VISIBLE);
                mAdapter.setIsLoadingNext(mGitHubUsersViewModel.getFirstUserIndex(), usersLoadingState.mUsers);
                break;
            case GitHubUsersViewModel.UsersLoadingState.IS_LOADING_PREV:
                mProgressBar.setVisibility(View.GONE);
                mUserList.setVisibility(View.VISIBLE);
                mAdapter.setIsLoadingPrev(mGitHubUsersViewModel.getFirstUserIndex(), usersLoadingState.mUsers);
                break;
            case GitHubUsersViewModel.UsersLoadingState.ERROR:
                if(usersLoadingState.mUsers!= null && usersLoadingState.mUsers.size()>0){
                    mAdapter.failedToLoadMore();
                }
                break;

            default:
                break;

        }
    }

    public interface GitHubUsersViewModelProvider{
        GitHubUsersViewModel getGitHubUsersViewModel();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(FirstVisibleUserIndexBundleKey, mFirstVisibleUserIndex);

    }
}
