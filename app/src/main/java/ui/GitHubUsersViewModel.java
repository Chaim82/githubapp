package ui;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.util.Log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

import models.GitHubUser;
import services.IGitHubServices;
import services.IServiceResponseListener;

public class GitHubUsersViewModel extends ViewModel {

    private IGitHubServices  mServices;
    private MutableLiveData<UsersLoadingState> mUserLoadingState = new MutableLiveData<UsersLoadingState>();
    private boolean isLoading;

    private int mFirstUserIndex = Integer.MAX_VALUE;
    private boolean mIsStarted;
    private final static int USERS_PAGE_SIZE = 30;

    private android.os.Handler mHandler = new android.os.Handler();

    private GitHubUsersViewModel(IGitHubServices  services) {
        mServices = services;
        mUserLoadingState.setValue(new UsersLoadingState(new ArrayList<GitHubUser>(),null,UsersLoadingState.IS_LOADING));
    }

    public void start(int loadFromUserIndex){
        if(mIsStarted)
            return;
        mIsStarted = true;
        int since = (loadFromUserIndex / USERS_PAGE_SIZE) * USERS_PAGE_SIZE;
        loadUser(since);
    }

    public LiveData<UsersLoadingState> getUserLoadingState(){
        return mUserLoadingState;
    }

    public int getFirstUserIndex(){
        return mFirstUserIndex;
    }

    public void loadPrev(){
        if(mFirstUserIndex == 0 || isLoading){
           return;
        }
        mUserLoadingState.setValue(new UsersLoadingState(mUserLoadingState.getValue().mUsers,null, UsersLoadingState.IS_LOADING_PREV));
        int since = (mFirstUserIndex - USERS_PAGE_SIZE < 0) ? 0 : mFirstUserIndex - USERS_PAGE_SIZE;
        loadUser(since);
    }

    public void loadNext(){
        if(isLoading)
            return;
        mUserLoadingState.setValue(new UsersLoadingState(mUserLoadingState.getValue().mUsers,null, UsersLoadingState.IS_LOADING_NEXT));
        loadUser(mUserLoadingState.getValue().mUsers.size());
    }

    private void loadUser(final int since){
        if(isLoading)
            return;
        isLoading = true;
        mServices.getGitHubUsers(new IServiceResponseListener<List<GitHubUser>>() {
            @Override
            public void onComplete(final List<GitHubUser> result) {

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        final List<GitHubUser> users = mUserLoadingState.getValue().mUsers;

                        if(mFirstUserIndex > since){
                            mFirstUserIndex = since;
                            users.addAll(0,result);
                        }
                        else{
                            users.addAll(result);
                        }
                        mUserLoadingState.setValue(new UsersLoadingState(users,result,UsersLoadingState.IS_READY));

                        isLoading = false;
                    }
                }, 10000);
            }

            @Override
            public void onError() {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isLoading = false;
                        mUserLoadingState.setValue(new UsersLoadingState(mUserLoadingState.getValue().mUsers,null,UsersLoadingState.ERROR));
                    }
                },10000);

            }
        },since);
    }

    public static class GitHubViewModelFactory extends ViewModelProvider.NewInstanceFactory {
        private IGitHubServices  mServices;

        public GitHubViewModelFactory(IGitHubServices  services) {
            mServices = services;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            return (T) new GitHubUsersViewModel(mServices);
        }
    }

    public static class UsersLoadingState{

        public static final int IS_LOADING = 0;
        public static final int IS_READY = 1;
        public static final int IS_LOADING_NEXT = 2;
        public static final int IS_LOADING_PREV = 3;
        public static final int ERROR = 4;

        @IntDef({IS_LOADING, IS_READY, IS_LOADING_NEXT, IS_LOADING_PREV, ERROR})
        @Retention(RetentionPolicy.SOURCE)
        public @interface LoadingState {}

        public final List<GitHubUser> mUsers;
        public final List<GitHubUser> mUsersFromLastUpdate;
        public final int mState;


        public UsersLoadingState(List<GitHubUser> users, List<GitHubUser> usersFromLastUpdate, @LoadingState int state) {
            this.mUsers = users;
            this.mState = state;
            this.mUsersFromLastUpdate = usersFromLastUpdate;
        }
    }
}
