package ui;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.test.githubapp.R;

import java.util.List;

import models.GitHubUser;
import services.GitHubServices;
import services.IServiceResponseListener;

public class GitHubUsersActivity extends AppCompatActivity implements GitHubUsersFragment.GitHubUsersViewModelProvider {

    private GitHubUsersViewModel mGitHubUsersViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.github_users_activity_layout);

        mGitHubUsersViewModel = ViewModelProviders.of(this, new GitHubUsersViewModel.GitHubViewModelFactory(new GitHubServices())).get(GitHubUsersViewModel.class);

        if(savedInstanceState == null){
            showUsersFragment();
        }
    }

    public GitHubUsersViewModel getGithubUserViewModel(){
        return mGitHubUsersViewModel;
    }

    private  void showUsersFragment(){
        startFragment(new GitHubUsersFragment());
    }

    private void startFragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.content_layout, fragment, fragment.getClass().getName()).addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    @Override
    public GitHubUsersViewModel getGitHubUsersViewModel() {
        return mGitHubUsersViewModel;
    }
}
