package ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.githubapp.R;

import java.util.ArrayList;
import java.util.List;

import models.GitHubUser;

public class GithubUsersAdapter extends RecyclerView.Adapter<GithubUsersAdapter.GitHubUserViewHolder> {

    private List<GitHubUser> mUsers = new ArrayList<>();
    private Context mContext;
    private GitHubUserViewHolder mLastHolder;
    private GitHubUserViewHolder mFirstHolder;
    private boolean mIsLoadingNext;
    private boolean mIsLoadingPrev;
    private int mIndexOfFirstElement;


    public GithubUsersAdapter(Context context){
        mContext = context;
    }

    @NonNull
    @Override
    public GitHubUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.github_user_list_item, parent, false);

        return new GitHubUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GitHubUserViewHolder holder, int position) {
        holder.onBind(position);
    }

     @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public void addUsers(int indexOfFirstUser, List<GitHubUser> allUsers , List<GitHubUser> newUsers){
        int size = mUsers.size();
        List<GitHubUser> users = size == 0 ? allUsers : newUsers;
        int indexToInsert = mIsLoadingPrev ? 0 : size;
        mUsers.addAll(indexToInsert, users);
        mIndexOfFirstElement = indexOfFirstUser;
        notifyItemRangeInserted(size, users.size());

        hideNextProgressBar();
        hidePrevProgressBar();
    }

    public void failedToLoadMore(){
        hideNextProgressBar();
    }

    private void hideNextProgressBar() {
        mIsLoadingNext = false;
        if(mLastHolder!= null){
            mLastHolder.setNextProgressBarVisibility(false);
        }
    }

    public void failedToLoadPrev(){
        hidePrevProgressBar();
    }

    private void hidePrevProgressBar() {
        mIsLoadingPrev = false;
        if(mFirstHolder!= null){
            mFirstHolder.setPrevProgressBarVisibility(false);
        }
    }

    public void setIsLoadingNext(int indexOfFirstUser, List<GitHubUser> allUsers){
        if(mLastHolder != null){
            mLastHolder.setNextProgressBarVisibility(true);
        }
        if(mUsers.size() == 0){
            addUsers(indexOfFirstUser,allUsers,null);
        }
        mIsLoadingNext = true;
    }
    public void setIsLoadingPrev(int indexOfFirstUser, List<GitHubUser> allUsers){
        if(mFirstHolder != null){
            mFirstHolder.setPrevProgressBarVisibility(true);
        }
        if(mUsers.size() == 0){
            addUsers(indexOfFirstUser,allUsers,null);
        }
        mIsLoadingPrev = true;
    }

    public class GitHubUserViewHolder extends RecyclerView.ViewHolder{

        TextView mUserNameTextView;
        ImageView mUserImage;
        ProgressBar mProgressBarPrev;
        ProgressBar mProgressBarNext;

        public GitHubUserViewHolder(View itemView) {
            super(itemView);
            mUserNameTextView = itemView.findViewById(R.id.nameTextView);
            mUserImage = itemView.findViewById(R.id.imageView);
            mProgressBarPrev = itemView.findViewById(R.id.progressBarPrev);
            mProgressBarNext = itemView.findViewById(R.id.progressBarNext);
        }

        private void onBind(int position){

      /*      if(position < mIndexOfFirstElement){
                clearFields();
                return;
            }*/

            GitHubUser user = mUsers.get(position);
            mUserNameTextView.setText(user.getLogin());
            Picasso.with(mContext).load(user.getAvatarUrl()).placeholder(R.drawable.ic_person_black_24dp).into(mUserImage);
            setProgressBarVisibility(mProgressBarNext,false);
            setProgressBarVisibility(mProgressBarPrev,false);

            if(position == mUsers.size()-1){
                mLastHolder = this;
                if(mIsLoadingNext){
                    setProgressBarVisibility(mProgressBarNext,true);
                }
                if(mFirstHolder == this){
                    mFirstHolder = null;
                }
            }
            else if(position == mIndexOfFirstElement){
                if(mIsLoadingPrev){
                    setProgressBarVisibility(mProgressBarPrev,true);
                }
                mFirstHolder = this;
                if(mLastHolder == this){
                    mLastHolder = null;
                }
            }
            else {
                if(mLastHolder == this){
                    mLastHolder = null;
                }
                if(mFirstHolder == this){
                    mFirstHolder = null;
                }
            }
        }

        private void clearFields(){
            mUserNameTextView.setText("") ;
            mUserImage.setImageDrawable(mContext.getDrawable(R.drawable.ic_person_black_24dp));
            setProgressBarVisibility(mProgressBarNext,false);
            setProgressBarVisibility(mProgressBarPrev,false);
        }

        private void setNextProgressBarVisibility(boolean isVisible){

            setProgressBarVisibility(mProgressBarNext, isVisible);
        }

        private void setPrevProgressBarVisibility(boolean isVisible){

            setProgressBarVisibility(mProgressBarPrev, isVisible);
        }

        private void setProgressBarVisibility(ProgressBar progressBar,boolean isVisible){
            if(isVisible){
                progressBar.setVisibility(View.VISIBLE);
            }
            else{
                progressBar.setVisibility(View.GONE);
            }
        }
    }
}
